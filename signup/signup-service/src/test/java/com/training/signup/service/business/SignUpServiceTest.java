package com.training.signup.service.business;

import com.training.authentication.signup.model.Customer;
import com.training.authentication.signup.model.CustomerResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
class SignUpServiceTest {

    @InjectMocks
    Customer customer;

    @InjectMocks
    SignUpService signUpService;

    @Test
    void signUpTestTokenResponseIfEmailAndPasswordNotNull() {

        customer.setEmail("isaac@email.com");

        customer.setPassword("password");

        CustomerResponse response = signUpService.signUp(customer);

        assertNotNull(response.getToken().getToken());

        assertNotNull(response.getToken().getExpirationDate());

    }

    @Test
    void signUpTestTokenResponseIfEmailNullPasswordNotNull() {

        customer.setPassword("password");

        CustomerResponse response = signUpService.signUp(customer);

        assertNull(response.getToken().getToken());

        assertNull(response.getToken().getExpirationDate());

    }

    @Test
    void signUpTestTokenResponseIfEmailNotNullPasswordNull() {

        customer.setEmail("isaac@email.com");

        CustomerResponse response = signUpService.signUp(customer);

        assertNull(response.getToken().getToken());

        assertNull(response.getToken().getExpirationDate());

    }

    @Test
    void signUpTestTokenResponseIfEmailNullAndPasswordNull() {

        CustomerResponse response = signUpService.signUp(customer);

        assertNull(response.getToken().getToken());

        assertNull(response.getToken().getExpirationDate());

    }

    @Test
    void signUpTestCustomerResponsePasswordIsNull() {

        CustomerResponse response = signUpService.signUp(customer);

        assertNull(response.getCustomer().getPassword());

    }

    @Test
    void signUpTestCustomerResponsePasswordIsNullAndToken() {

        customer.setEmail("isaac@email.com");

        CustomerResponse response = signUpService.signUp(customer);

        assertNull(response.getCustomer().getPassword());

        assertNull(response.getToken().getToken());

    }

    @Test
    public void getTokenEmailAndPasswordNotNullTest() {

        customer.setEmail("a@email.com");

        customer.setPassword("password");

        assertNotNull(customer.getEmail());

        assertNotNull(customer.getPassword());

    }

}