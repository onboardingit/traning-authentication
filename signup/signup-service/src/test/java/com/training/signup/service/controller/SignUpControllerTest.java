package com.training.signup.service.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class SignUpControllerTest {

    private MockMvc mvc;

    @Test
    void signUp() {
    }

    @Test
    void healthCheck() throws Exception {

        SignUpController controller = new SignUpController(); // Arrange

        String response = controller.healthCheck(); // Act

        assertEquals("Hello! i'm OK", response);// Assert

    }
}