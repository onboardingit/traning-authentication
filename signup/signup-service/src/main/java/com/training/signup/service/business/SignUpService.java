package com.training.signup.service.business;

import com.training.authentication.signup.model.Customer;
import com.training.authentication.signup.model.CustomerResponse;
import com.training.authentication.signup.model.Token;
import org.springframework.stereotype.Service;

@Service
public class SignUpService {

    public CustomerResponse signUp(Customer customer) {

        Token tokenData = getToken(customer);

        CustomerResponse response = new CustomerResponse();

        customer.setPassword(null);

        response.setCustomer(customer);

        response.setToken(tokenData);

        return response;

    }

    private Token getToken(Customer customer) {

      Token token = new Token();

      if (customer.getEmail() == null || customer.getPassword() == null){

        token.setToken(null);

        token.setExpirationDate(null);

        return token;
      }

      token.setToken("sdfadsfasdfa");

      token.setExpirationDate("22/2/2021");

      return token;

    }

}
