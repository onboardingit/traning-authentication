package com.training.signup.service.controller;

import com.training.authentication.signup.api.SignUpApi;
import com.training.authentication.signup.model.Customer;
import com.training.authentication.signup.model.CustomerResponse;
import com.training.signup.service.business.SignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping("/sign-up")
public class SignUpController implements SignUpApi {

  @Autowired
  private SignUpService signUpService;

  @PostMapping
  public ResponseEntity<CustomerResponse> signUp(@RequestBody Customer customer) {

    CustomerResponse response = signUpService.signUp(customer);

    return ResponseEntity.ok(response);

  }

  @GetMapping("/health-check")
  public String healthCheck() {

    return "Hello! i'm OK";

  }

}
