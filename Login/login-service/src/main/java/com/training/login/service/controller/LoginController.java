package com.training.login.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.authentication.login.api.LoginApi;
import com.training.login.service.business.LoginServiceBusiness;

@RestController
@RequestMapping("/login")
public class LoginController implements LoginApi {

  @Autowired
  LoginServiceBusiness loginserviceBusiness;

  @GetMapping("/health-check")
  public String healthCheck() {

    return "Hello World from Login ";
  }

  @GetMapping("/health-check-business")
  public String healthCheckBusiness() {

    return loginserviceBusiness.healthCheck();
  }

}
