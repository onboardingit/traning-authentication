package com.training.login.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.training.login.service.business.LoginServiceBusiness;

@WebMvcTest
public class WebLayerTest {

  /**
   * In this test, Spring Boot instantiates only the web layer rather than the whole context. In an
   * application with multiple controllers, you can even ask for only one to be instantiated by
   * using, for example, @WebMvcTest(HomeController.class).
   * 
   */

  @Autowired
  private MockMvc mockMvc;

  //Necesaria dependenica para instanciar "por debajo" el restController 
  @MockBean
  private LoginServiceBusiness loginServiceBusiness;
  
  @Test
  public void shouldReturnDefaultMessage() throws Exception {

    String uri = "/login/health-check";

    this.mockMvc.perform(get(uri)).andDo(print()).andExpect(status().isOk())
        .andExpect(content().string(containsString("Hello World from Login")));
  }

}
