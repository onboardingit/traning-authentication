package com.training.login.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.training.login.service.business.LoginServiceBusiness;

@WebMvcTest
public class WebMockTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private LoginServiceBusiness loginServiceBusiness;

  @Test
  public void shouldReturnDefaultMessageFromService() throws Exception {

    String uri = "/login/health-check-business";

    String testData = "HOLA ESTE ES MI MOCK";

    when(loginServiceBusiness.healthCheck()).thenReturn(testData);

    this.mockMvc.perform(get(uri)).andDo(print()).andExpect(status().isOk())
        .andExpect(content().string(containsString(testData)));
  }

}
