package com.training.authorization.service.repo;

import org.springframework.stereotype.Repository;

import com.training.authentication.authorization.model.Customer;

@Repository
public interface CustomerInterface{
  
  Customer findByEmail(String email);

}
