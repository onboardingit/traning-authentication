package com.training.authorization.service.repo;

import org.springframework.stereotype.Repository;

import com.training.authentication.authorization.model.Token;

@Repository
public interface TokenInterface /*extends JpaRepository<Token, Integer>*/{
  
  Token findByToken(String Token);

}