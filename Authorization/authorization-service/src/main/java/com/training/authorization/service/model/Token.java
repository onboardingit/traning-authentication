package com.training.authorization.service.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CLASE MODELO TEMPORAL * 
 * @author rfeliz *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Token {

  private Long id;

  private Long userId;

  private String token;

  private Date expirationDate;

  private String expirationDateAuth;

  public Token(Long userId, String token, String expirationDate) {
    this.userId = userId;
    this.token = token;
    this.expirationDateAuth = expirationDate;
  }
  
  public Token(Long userId, String token, Date expirationDate) {
    this.userId = userId;
    this.token = token;
    this.expirationDate = expirationDate;
  }
  
  public Token(Long id, Long userId, String token, Date expirationDate) {
    this.id = id;
    this.userId = userId;
    this.token = token;
    this.expirationDate = expirationDate;
  }

}


