package com.training.authorization.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class JWTUtil {
  
  private String SECRET_KEY = "secret";
  private long TIME_MILLIS = 300000;
  
  public String extractIdCustomer(String token) {
    
    //Se obtiene el id del usario del token
    return extractClaim(token).getSubject();
    
  }

  private Claims extractClaim(String token) {
    
    final Claims claims = extractAllClaims(token);  
    return claims; 
    
  }
  
  private Claims extractAllClaims(String token) {
    
    //Se asigna la clave de la firma al token
    return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    
  }
  
  private Boolean isTokenExpired(String token) {
    
    //Se verifica si el token ha expirado
    return extractExpiration(token).before(new Date());    
    
  }
  
  public Date extractExpiration(String token) {
    
    //Se obtiene la fecha de expiracion del token
    return extractClaim(token).getExpiration();
    
  }
  
  public String generateToken(Long idUser) {
    
    //Se llama al metodo crear token
    Map<String, Object> claims = new HashMap<String, Object>();
    return createToken(claims, idUser);
    
  }
  
  private String createToken(Map<String, Object> claims, Long idUser) {
    
    //Se contruyen los valores para el token
    return Jwts.builder().setClaims(claims).setSubject(String.valueOf(idUser)).setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + TIME_MILLIS))
        .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
  }
  
  public Boolean validateToken(String token) {
 
    //Verifico que no haya expirado
    return (!isTokenExpired(token));    
    
  }

}
