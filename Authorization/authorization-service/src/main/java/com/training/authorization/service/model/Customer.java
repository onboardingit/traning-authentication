package com.training.authorization.service.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *  CLASE MODELO TEMPORAL *
 * @author rfeliz
 */
@Data
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Customer {

  /* Contiene el id del usuario */
  private Long id;

  /* Contiene el nombre del usuario */
  private String firstName;

  /* Contiene el apellido del usuario */
  private String lastName;

  /* Contiene la identificación personal única del usuario */
  private String identityId;

  /* Contiene el tipo de identificación del usuario del usuario ejm: cédula, pasaporte, licencia */
  private String identityType;

  /* Contiene el número telefonico vinculado del usuario */
  private String phoneNumber;

  /* Contiene el correo electrónico vinculado del usuario */
  private String email;

  /* Contiene la password encriptada del usuario */
  private String password;

  public Customer(Long id, String firstName, String lastName, String identityId, String identityType,
      String phoneNumber, String email, String password) {

	this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.identityId = identityId;
    this.identityType = identityType;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.password = password;
  }

}
