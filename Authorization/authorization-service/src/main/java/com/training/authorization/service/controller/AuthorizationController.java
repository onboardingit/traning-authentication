package com.training.authorization.service.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.authentication.authorization.api.AuthorizationApi;
import com.training.authentication.authorization.model.Customer;

@RestController
public class AuthorizationController implements AuthorizationApi {

  public ResponseEntity<Customer> refresh(@RequestBody Customer customer) {

    // getRequest().ifPresent(request -> {
    // for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
    // if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
    // String exampleString =
    // "{ \"firstName\" : \"firstName\", \"lastName\" : \"lastName\", \"password\" : \"password\",
    // \"phoneNumber\" : \"phoneNumber\", \"userStatus\" : 0, \"identityType\" : \"identityType\",
    // \"identityId\" : \"identityId\", \"email\" : \"email\" }";
    // ApiUtil.setExampleResponse(request, "application/json", exampleString);
    // break;
    // }
    // }
    // });
    
    //TODO: LOGICA DE NEGOCIO
    
    //AuthorizationBusiness.refresh();
    
    return ResponseEntity.ok().build();
  }

}
