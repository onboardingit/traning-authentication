package com.training.authorization.service.business;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.training.authorization.service.exception.TokenBusinessException;
import com.training.authorization.service.model.Customer;
import com.training.authorization.service.model.Token;
import com.training.authorization.service.repo.CustomerInterface;
import com.training.authorization.service.repo.TokenInterface;
import com.training.authorization.util.JWTUtil;

/*import com.training.authentication.authorization.model.Customer;
import com.training.authentication.authorization.model.Token;*/

@Service
public class AuthorizationBusiness {
	
	 @Autowired
	  private JWTUtil jwtUril;

	  @Autowired
	  private TokenInterface tokenInterface;

	  @Autowired
	  private CustomerInterface customerInterface;	  
	 
	  public AuthorizationBusiness(TokenInterface tokenInterface) {
			this.tokenInterface = tokenInterface;
	  }
	  
	  public AuthorizationBusiness(CustomerInterface customerInterface) {
			this.customerInterface = customerInterface;
	  }

	  public Token getToken(String customerEmail) throws TokenBusinessException {
	    
	    // Genero el token recibiendo por el email del usuario por request
	    if (customerEmail == null) {
	      throw new TokenBusinessException("1030", "Error al recibir el email",
	          HttpStatus.BAD_REQUEST);
	    }
	    
	    // Busco el id del usuario por el email del usuario por request
	    Customer customerCreated = getCustomer(customerEmail);

	    // Guardo el token
	    Token token = saveToken(customerCreated);

	    return token;
	  }

	  public Token saveToken(Customer customer) throws TokenBusinessException {

	    if (customer == null) {
	      throw new TokenBusinessException("1040", "Error con los datos del usuario",
	          HttpStatus.EXPECTATION_FAILED);
	    }

	    //Se genera el token con el id del usuario del email
	    JWTUtil jwtUril = new JWTUtil();
	    String stringToken = jwtUril.generateToken(customer.getId());
	    
	    //Se obtiene la fecha de expiracion del token generado
	    Date dateExpiration = jwtUril.extractExpiration(stringToken);
	    
	    //Se crea un nuevo objeto token
	    Token newToken = new Token(customer.getId(), stringToken, dateExpiration);
	    
	    //Se guarda el objeto token en bd
	    //Token tokenSaved = tokenInewTokennterface.save(newToken);
	    
	    //Se crea un token con los datos al devolver
	    Token tokenResponse = new Token(customer.getId(), newToken.getToken(), String.valueOf(jwtUril.getTIME_MILLIS()));    

	    return tokenResponse;
	  }

	  public Customer validateToken(String tokenRequest) throws TokenBusinessException {

	    if (tokenRequest == null) {
	      throw new IllegalStateException("Atributos faltantes");
	    }

	    JWTUtil jwtUril = new JWTUtil();
	    Boolean isValidToken = jwtUril.validateToken(tokenRequest);

	    if (!isValidToken) {
	      throw new TokenBusinessException("1060", "Token invalido",
	        HttpStatus.EXPECTATION_FAILED);
	    }

	    /*Del token del request se obtiene el id del usuario      * 
	     * para crear un nuevo token y fecha de expiracion     * 
	     * */
	    JWTUtil jwtUril2 = new JWTUtil();
	    String idCustomer = jwtUril2.extractIdCustomer(tokenRequest);

	    Customer customer = new Customer();

	    customer.setId(Long.valueOf(idCustomer));
	    
	    return customer;

	  }

	  public Token getRefresh(String tokenRequest) throws TokenBusinessException {

	    if (tokenRequest == null) {
	      throw new IllegalStateException("Atributos faltantes");
	    }

	    JWTUtil jwtUril = new JWTUtil();
	    Boolean isValidToken = jwtUril.validateToken(tokenRequest);
	    if (!isValidToken) {
	      throw new TokenBusinessException("1060", "Token invalido",
	          HttpStatus.EXPECTATION_FAILED);
	    }

	    // Del token del request se obtiene el id del usuario para crear un nuevo token y fecha de
	    // expiracion

	    //Obtengo el id del usuario del token recibido por request
	    String idCustomer = jwtUril.extractIdCustomer(tokenRequest);
	    
	    //Se genera un nuevo token
	    String newToken = jwtUril.generateToken(Long.valueOf(idCustomer));
	    
	    //Se obtiene la fecha de expiracion del nuevo token
	    Date newDate = jwtUril.extractExpiration(newToken);

	    //Se un objecto token con los datos del nuevo token
	    Token refreshedToken =
	        new Token(1L, Long.valueOf(idCustomer), newToken, newDate);

	    //Se atualiza en bd
	    //Token tokenUpdated = tokenInterface.save(refreshedToken);
	    
	    //Se crea otro token con los datos al devolver en el response
	    Token tokenResponse = 
	        new Token(refreshedToken.getUserId(), refreshedToken.getToken(), String.valueOf(jwtUril.getTIME_MILLIS()));

	    return tokenResponse;

	  }

	  public Customer getCustomer(String customerEmail) throws TokenBusinessException {

	    if (customerEmail == null) {
	      throw new TokenBusinessException("1030", "Error al recibir el email",
	          HttpStatus.BAD_REQUEST);
	    }
	    
	    Customer customer = new Customer();
	    customer.setId(1L);
	    customer.setFirstName("Regina");
	    customer.setLastName("Feliz");
	    customer.setIdentityId("58632");
	    customer.setIdentityType("V");
	    customer.setPhoneNumber("5985324485");
	    customer.setEmail(customerEmail);
	    customer.setPassword("1234");
	    
	    return customer;
	  }

}