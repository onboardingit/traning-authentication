package com.training.authorization.business;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.training.authorization.service.business.AuthorizationBusiness;
import com.training.authorization.service.exception.TokenBusinessException;
import com.training.authorization.service.model.Customer;
import com.training.authorization.service.model.Token;
import com.training.authorization.service.repo.CustomerInterface;
import com.training.authorization.service.repo.TokenInterface;

public class AuthorizationBusinessMockTest {	
	
	@Test
	public void getToken() {
		
		CustomerInterface customerInterface = mock(CustomerInterface.class);
		
		AuthorizationBusiness authorizationService = new AuthorizationBusiness(customerInterface);		

		Token token = new Token(1L, "", "300000"); 
		
		Token tokenExpectedResponse = null;
		
		try {
			
			tokenExpectedResponse = authorizationService.getToken("regina@test.com");				
			
		} catch (TokenBusinessException e) {
			
			//fail("El email del usuario no existe");
			assertThat(e).isInstanceOf(TokenBusinessException.class).hasMessage("Usuario no encontrado");
			
		}
		
		System.out.println("*** Token generado: " + tokenExpectedResponse.getToken());
		
		//En este caso se debe verificar que token generado sea igual al de bd.Pero en este caso, el token generado debe ser el mismo user id del email
		Assertions.assertThat(tokenExpectedResponse.getUserId()).isEqualTo(token.getUserId());
		
		//El Token no debe ser Nulo
		assertNotNull(tokenExpectedResponse, "Error generar el token");
		
	}
	
	@Test
	public void getValidateToken() {
		
		TokenInterface tokenInterface = mock(TokenInterface.class);
		
		AuthorizationBusiness authorizationService = new AuthorizationBusiness(tokenInterface);
		
	    Customer customerExpected = new Customer(1L, "Regina", "Feliz", "58632", "V", "5985324485", "regina@test.com", "1234");
		
		Customer customerResponse = null;
		
		try {
			
			customerResponse = 
					authorizationService
					.validateToken("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwiZXhwIjoxNjEyNDgyMTUzLCJpYXQiOjE2MTI0ODE4NTN9.WuHCl8jK6aIDrEAYue_62LfmF2db72ivMFF1GuAECKA");

		} catch (TokenBusinessException e) {
			
			//Excepcion por que le token no es valido porque ha expirado o el El email del usuario no existe			
			assertThat(e).isInstanceOf(TokenBusinessException.class).hasMessage("Token/Usuario invalido");
			
		}
		
		//El usuario obtenido debe ser igual al id del usuario del token
		Assertions.assertThat(customerResponse.getId()).isEqualTo(customerExpected.getId());
		
		//El Customer no debe ser Nulo
		assertNotNull(customerResponse, "Error al obtener el cliente");
		
	}
	
	@Test
	public void getRefresh() {
		
		TokenInterface tokenInterface = mock(TokenInterface.class);
		
		AuthorizationBusiness authorizationService = new AuthorizationBusiness(tokenInterface);
		
		Token oldToken = new Token(1L, "J5S5E2F5A8AA5TGHYRWX5", "300000");
		
		Token tokenResponse = null;
		
		try {

			tokenResponse = 
					authorizationService
					.getRefresh("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwiZXhwIjoxNjEyNDgyMzU5LCJpYXQiOjE2MTI0ODIwNTl9.lRSbww2ODtPqgSpffp6AEi16A3mAMizmFXMoj9R1fsQ");			
			
		} catch (TokenBusinessException e) {
			
			//Exepcion por el token no es valido para actualizar		
			assertThat(e).isInstanceOf(TokenBusinessException.class).hasMessage("Token invalido, no actualizado");
			
		}
		
		//El token obtenido debe ser diferente al recibido por resquet.
		Assertions.assertThat(tokenResponse.getToken()).isNotEqualTo(oldToken.getToken());
		
		//El token generado no debe ser nulo
		assertNotNull(tokenResponse, "Error al actualizar el token");
		
	}
	
}
